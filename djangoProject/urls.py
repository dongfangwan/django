"""djangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from app01 import views

# from app02 import views

urlpatterns = [
    # path("admin/", admin.site.urls)
    #www.xxx.com/index --->函数 从app01导入index函数
     path("index/", views.index),
     #用户列表页面
     path("user/list/",views.user_list),
     #写一个tpl功能页面 函数也叫tpl
     path("tpl",views.tpl),

     #联通新闻中心
     path("news",views.news),

     #用户登录注册
     path("login/",views.login),

     #用户管理
     path("user/info",views.user_info),
     #用户管理-添加
     path("user/add",views.info_add),

]

from django.http import HttpResponse
from django.shortcuts import render , redirect

from app01.models import User_Info


# Create your views here.

def index(request):
    return HttpResponse("欢迎使用")

def user_list(request):
    #去app目录下 templates寻找html文件  根据app注册顺序 按顺序找temples
    return render(request,"user_list.html")


def tpl(request):
    name="孙悟空"
    role=["管理员","CEO","宝安"]
    user_info={"name":"阿龙龙","salary":100000,"role":"CTO"}
    return render(request,"tpl.html",
                  {
                      "n1":name,
                      "n2":role,
                      "n3":user_info

                   }
                  )

def news(req):
    #临时去别的地方爬数据

    import requests
    res = requests.get("http://www.chinaunicom.com/news/list202310.html")
    date_code = res.status_code
    try:
        date_list=res.content.decode("utf-8")
        print(date_code)
    except Exception as e:
        print("请求错误")


    return render(req,"news.html",{
        "date_code":date_code,
        "date_list":date_list


    })

def login(request):

    if request.method=="GET":
        return render(request,"login.html")

    print(request.POST)
    user=request.POST.get('user')
    pwd=request.POST.get('password')
    if user=='root' and pwd=='123':

        # return HttpResponse("登录成功")
        return redirect("http://www.chinaunicom.com/news/list202310.html")
    #上面是登录成功 执行if代码里的  换行跟if对齐是没成功的  可以省略else了
    # return HttpResponse("登录失败")
    return render(request,"login.html",{"error_msg":"用户名或者密码错误"} )


def user_info(req):
    #1.获取数据库中所有的用户信息
    data_list=User_Info.objects.all()
    print(data_list)
    #ba data_list参数传给html模板参数
    return render(req,"user_info.html",{"data_list":data_list})


def info_add(req):
    if req.method =="GET":
        return render(req,'info_add.html')
    user=req.POST.get("user")


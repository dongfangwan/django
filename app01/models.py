from django.db import models


# Create your models here.


class User_Info(models.Model):
    name=models.CharField(max_length = 32)
    password=models.CharField(max_length = 64)
    age=models.IntegerField(default =0)
    size=models.CharField(max_length =32,null =True,blank=True)

class Department(models.Model):
    title=models.CharField(max_length = 32)
    salary=models.IntegerField(default = 666666)

# class Role(models.Model):
#     # caption=models.CharField(max_length = 64)

#对类的数据进行操作 即对字段数据操作 增删改查

User_Info.objects.create(name = "小万",password = 123456,age=30,size = 9999)
User_Info.objects.create(name = "小fa",password = '123dfa456',age=320,size = 222)
User_Info.objects.create(name = "小ba",password = '1234adfad56',age=310,size = 99979)
User_Info.objects.create(name = "小c",password = '1234sfasdfasf56',age=300,size = 99799)
# User_Info.objects.filter(id=2).delete()
# User_Info.objects.all().delete()
